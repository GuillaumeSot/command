<?php 
class Command implements CommandInterface {
    private $console;
    public function consoleOn($console){
        $this->console = $console;
    }
    public function execute(){
        $this->console->on();
    }
}